# q2-readorient

A plugin for Qiime2 to : 
* Reorient features or reads using primer sequences orientation
* Optionnaly trim primers from features or reads

## Rationale

When preparing Illumina libraries from PCR amplicons of SSU rDNA, and depending on the experimental setup, DNA might be ligated in both orientation, leading to two sets of reads : one set reading in the sense ("coding") direction and the other in the antisense direction.

During the process of clusetring/denoising reads into OTUs or ASVs, not correcting this orientation will lead to OTUs duplicates in both direction. Phylogenetic analysis of thoses OTUs immediatelly show a duplication of the same OTUs in two separate clades.
If not adressed, this duplication will eventually be interpreted as twice the actual diversity in OTUs.

This plugin can be used at various step of the analysis to put back all reads/ASVs/OTUs in the correct direction while also updating OTU tables if needed.

## Installation
For the moment, the plugin is not distributed via Anaconda repositores.

To install q2-readorient, you can clone this git repository, load the conda environement where you intalled qiime2 and run the local copy of pip with the command `pip install /path/to/cloned/repo/`.

Finally, you must update qiime2 cache with the command `qiime dev refresh-cache`.

## Usage
#### Reorient Features With Table

    Usage: qiime readorient features-w-table [OPTIONS]

    Reads denoising and asv merging made with dada2 does not take into account
    the orientation for the reads leading to identical asv in opposite
    directions, and thusartificially doubling the number of OTUs in features
    table. This plugin offers a solution by reorienting asv and optionnally
    clustering identical asv

    Inputs:
    --i-features ARTIFACT FeatureData[Sequence]
                            The feature sequences to be reoriented.    [required]
    --i-features-table ARTIFACT FeatureTable[Frequency]
                            Feature table, will be used for re-clustering.
                                                                        [required]
    Parameters:
    --p-primer-fwd TEXT    Forward primer sequence used for amplification
                                                                        [required]
    --p-primer-rev TEXT    Reverse primer sequence used for amplification
                                                                        [required]
    --p-trim-primers / --p-no-trim-primers
                            Trim primers sequence from feature    [default: True]
    --p-keep-unkn / --p-no-keep-unkn
                            Keep features for which orientation could ot be
                            determined (not compatible with trim-primers)
                                                                [default: False]
    --p-dereplicate / --p-no-dereplicate
                            Dereplicate identical features after reorientation
                                                                [default: True]
    Outputs:
    --o-reoriented-features ARTIFACT FeatureData[Sequence]
                            Features sequences after reorientation  (optionally
                            dereplicated)                              [required]
    --o-derep-feature-table ARTIFACT FeatureTable[Frequency]
                            Features table after reorientation  (optionally
                            dereplicated)                              [required]
    --o-readorient-stats ARTIFACT FeatureData[ReadOrientStats]
                            Report containing reorentation and dereplication
                            stats                                      [required]
    Miscellaneous:
    --output-dir PATH      Output unspecified results to a directory
    --verbose / --quiet    Display verbose output to stdout and/or stderr
                            during execution of this action. Or silence output if
                            execution is successful (silence is golden).
    --examples             Show usage examples and exit.
    --citations            Show citations and exit.
    --help                 Show this message and exit.

## Documentation
🚧

## Contact
If you encounter issues, please e-mail me at philippe(dot)deschamps(at)universite-paris-saclay(dot)fr
