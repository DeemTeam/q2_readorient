#  ---------------------- ReadOrient -------------------------  #
#  A plugin for Qiime2 to reorient amplicon derived features    #
#  using the primer sequences that were used for amplification  #
#                                                               #
#  Author : Philippe Deschamps <philippe.fields@gmail.com>      #
#                                                               #
#  Distributed under the license terms of Qiime2                #
#  https://qiime2.org/                                          #
#  -----------------------------------------------------------  #

from importlib_resources import files as import_assets

__assets__ = import_assets('q2_readorient') / 'assets'

from q2_readorient._using_primers import (
    features_w_table_primer
)
from q2_readorient._using_align import (
    features_w_table_align
)

from ._version import get_versions

__version__ = get_versions()['version']
del get_versions
