#  ---------------------- ReadOrient -------------------------  #
#  A plugin for Qiime2 to reorient amplicon derived features    #
#  using the primer sequences that were used for amplification  #
#                                                               #
#  Author : Philippe Deschamps <philippe.fields@gmail.com>      #
#                                                               #
#  Distributed under the license terms of Qiime2                #
#  https://qiime2.org/                                          #
#  -----------------------------------------------------------  #

from biom import Table, load_table
from pathlib import Path
from numpy import add


def load_biom_table(filepath: Path) -> Table:
    """
    Loads a biom file into a biom Table object
    :param filepath: a file Path containing biom data
    :return: The Biom Table
    """
    return load_table(filepath)


def get_seq_to_count(biom_table: Table) -> dict:
    """

    :param biom_table: The biom table object
    :return: {seq : count}
    """
    return {seq: int(sum(count)) for count, seq, metadata in biom_table.iter(axis='observation')}


def dereplicate_table(biom_table: Table, clusters: dict) -> Table:
    """

    :param biom_table: the table to dereplicate
    :param clusters:
    :return:
    """
    out_table = {}

    for cluster, members in clusters.items():
        out_table[cluster] = biom_table.data(cluster, axis='observation', dense=True)
        for m in members:
            # if m != cluster:
            out_table[cluster] = add(out_table[cluster],
                                     biom_table.data(m, axis='observation', dense=True))

    return Table(list(out_table.values()), list(clusters.keys()), biom_table.ids())
