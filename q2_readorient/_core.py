#  ---------------------- ReadOrient -------------------------  #
#  A plugin for Qiime2 to reorient amplicon derived features    #
#  using the primer sequences that were used for amplification  #
#                                                               #
#  Author : Philippe Deschamps <philippe.fields@gmail.com>      #
#                                                               #
#  Distributed under the license terms of Qiime2                #
#  https://qiime2.org/                                          #
#  -----------------------------------------------------------  #

from q2_types.feature_data import DNAFASTAFormat
from qiime2 import Metadata
import skbio
import biom
import logging

from q2_readorient._types import Primer, Direction, Ends, ReadOrientStatsObj
from q2_readorient._vsearch import _generate_input_file, _cluster_100
from q2_readorient._biom import dereplicate_table
