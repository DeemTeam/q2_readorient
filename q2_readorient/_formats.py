#  ---------------------- ReadOrient -------------------------  #
#  A plugin for Qiime2 to reorient amplicon derived features    #
#  using the primer sequences that were used for amplification  #
#                                                               #
#  Author : Philippe Deschamps <philippe.fields@gmail.com>      #
#                                                               #
#  Distributed under the license terms of Qiime2                #
#  https://qiime2.org/                                          #
#  -----------------------------------------------------------  #

from csv import reader
import qiime2.plugin.model as model
from qiime2.plugin import ValidationError


class UsearchClusterFmt(model.TextFileFormat):
    """Implements the USEARCH Cluster (UC) a tab-separated text file
       defined by R. Edgar @ https://drive5.com/usearch/manual/opt_uc.html
    """
    def _dict_clusters(self) -> dict:
        """
        Reads the UC fie into a dictionary
        :return: dict {'centroid' : [members]}
        """
        _cluster_data = {}
        with open(str(self)) as fh:
            csv_reader = reader(fh, delimiter='\t')
            for row in csv_reader:
                if row[0] == 'S':
                    _cluster_data[row[8].split(';')[0]] = []
                elif row[0] == 'H':
                    _cluster_data[row[9].split(';')[0]].append(row[8])
        return _cluster_data

    def _validate_(self, *args):
        """
        helper function for input file format validation
        :return: None
        """
        with open(str(self)) as fh:
            csv_reader = reader(fh, delimiter='\t')
            for i, row in enumerate(csv_reader):
                if len(row) != 10:
                    raise ValidationError(
                        'Incorrect number of fields detected on line %d.'
                        ' Should be exactly 10.' % (i + 1))


class ReorientStatsFmt(model.TextFileFormat):
    # TODO Check if a validation is necessary.
    def _validate_(self, *args):
        pass


ReadOrientStatsDirFmt = model.SingleFileDirectoryFormat(
    'ReadOrientStatsDirFmt', 'readorient_stats.tsv', ReorientStatsFmt)