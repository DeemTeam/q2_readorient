#  ---------------------- ReadOrient -------------------------  #
#  A plugin for Qiime2 to reorient amplicon derived features    #
#  using the primer sequences that were used for amplification  #
#                                                               #
#  Author : Philippe Deschamps <philippe.fields@gmail.com>      #
#                                                               #
#  Distributed under the license terms of Qiime2                #
#  https://qiime2.org/                                          #
#  -----------------------------------------------------------  #
# Imports from libraries
from subprocess import run
import skbio
from typing import List
# Imports from Qiime
from q2_types.feature_data import DNAFASTAFormat


def _mafft_prepare_input_file(amplicon_file: DNAFASTAFormat,
                              ref_seq: List[skbio.sequence.DNA],
                              splitting_after: int = 500
                              ) -> List[DNAFASTAFormat]:
    """
    Split amplicon file in as many chunks of _splitting_after sequences
    :param amplicon_file:
    :param ref_seq:
    :param splitting_after: the number of sequences to put in each output file
    :return:
    """

    def _write_file(_ref_seq, _tempo_seq_container):
        _result = DNAFASTAFormat()
        with open(str(_result), 'w') as _temp_fasta:
            for s in _ref_seq:
                _temp_fasta.write('>{}\n{}\n'.format(s.metadata['id'], str(s)))
            for s in _tempo_seq_container:
                _temp_fasta.write('>{}\n{}\n'.format(s.metadata['id'], str(s)))
        return _result

    # ----- Generating input files ----- #
    _input_files = []  # Will store output files
    _tempo_seq_container = []  # Will store sequence before writing in files
    _seq_iterator = 0  # Will count seqs before writing them when @ _splitting_after
    for seq in skbio.io.read(str(amplicon_file), constructor=skbio.DNA, format='fasta'):
        _seq_iterator += 1
        _tempo_seq_container.append(seq)
        if _seq_iterator == splitting_after:
            # outputs sequences
            _input_files.append(_write_file(ref_seq, _tempo_seq_container))
            # reset tempo data
            _tempo_seq_container = []
            _seq_iterator = 0

    if len(_tempo_seq_container) > 0:
        _input_files.append(_write_file(ref_seq, _tempo_seq_container))

    return _input_files


def _mafft_adjust_direction(input_f: DNAFASTAFormat,
                            output_f: DNAFASTAFormat
                            ) -> None:
    cmd = ['mafft',
           '--adjustdirection',
           '--quiet',
           str(input_f),
           '>',
           str(output_f)]
    _completed_process = run(' '.join(cmd), shell=True)
    if _completed_process.returncode != 0:
        raise RuntimeError("Command Failed : {}".format(' '.join(cmd)))
