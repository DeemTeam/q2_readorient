#################################################################
#                                                               #
#  ---------------------- ReadOrient -------------------------  #
#  A plugin for Qiime2 to reorient amplicon derived features    #
#  using the primer sequences that were used for amplification  #
#                                                               #
#  Author : Philippe Deschamps <philippe.fields@gmail.com>      #
#                                                               #
#  Distributed under the license terms of Qiime2                #
#  https://qiime2.org/                                          #
#                                                               #
#################################################################

# Imports from libraries
from re import compile as re_compile
from enum import Enum
from skbio import DNA
from csv import DictWriter
from pandas import DataFrame
from pathlib import Path
# Imports from Qiime
from qiime2.plugin import SemanticType
from q2_types.feature_data import FeatureData
# from q2_types.feature_data import DNAFASTAFormat
# Internal Imports
from q2_readorient import __assets__


ReadOrientStats = SemanticType('ReadOrientStats',
                               variant_of=FeatureData.field['type'])

AMBIGUITIES = {'A': 'A', 'T': 'T', 'C': 'C', 'G': 'G', 'M': '[AC]',
               'R': '[AG]', 'W': '[AT]', 'S': '[CG]', 'Y': '[CT]',
               'K': '[GT]', 'V': '[ACG]', 'H': '[ACT]', 'D': '[AGT]',
               'B': '[CGT]', 'X': '[GATC]', 'N': '[GATC]'}


class Primer(object):
    """
    Abstraction of a primer sequence
    """

    def __init__(self, name: str, sequence: str):
        self.name = name
        self.raw_sequence = DNA(sequence)
        self.sens_pat = re_compile("({})".format(
            Primer.resolve_degenerate(
                self.raw_sequence
            )
        )
        )
        self.anti_pat = re_compile("({})".format(
            Primer.resolve_degenerate(
                self.raw_sequence.reverse_complement()
            )
        )
        )

    @staticmethod
    def resolve_degenerate(seq: DNA) -> str:
        """
        Converts IUPAC letters of degenerate base into a regex compatible string of all possible corresponding bases
        :param seq: the sequence to convert
        :return: str
        """
        return ''.join([AMBIGUITIES[N] if N in AMBIGUITIES else N for N in str(seq)])


class ReadOrientStatsObj(object):
    """
    Abstraction of the stats outputted by the plugin. Ensure construction and csv (Metadata) output
    """

    def __init__(self):
        self.table = []
        self.columns = ['feature-id', 'orientation', 'primer_fwd', 'primer_rev', 'trimAt5p', 'trimAt3p', 'deleted',
                        'clustered_with']
        self.num_seq_after_dereplication = False

    @staticmethod
    def row_template():
        return {'feature-id': None,
                'orientation': None,
                'primer_fwd': None,
                'primer_rev': None,
                'trimAt5p': False,
                'trimAt3p': False,
                'deleted': False,
                'clustered_with': 'None'
                }

    def add_row(self,
                name: str,
                orientation,
                primer_fwd,
                primer_rev,
                trimAt5p: bool,
                trimAt3p: bool,
                deleted: bool,
                cluster_with: str):
        self.table.append([name,
                           orientation,
                           primer_fwd,
                           primer_rev,
                           trimAt5p,
                           trimAt3p,
                           deleted,
                           cluster_with])

    def recap(self):
        out = [
            ['Computed Seq', len(self.table)],
            ['Total Sens', sum([1 for a in self.table if a[1] == 'sens'])],
            ['Total Anti', sum([1 for a in self.table if a[1] == 'anti'])],
            ['Total 5p Ends Found',
             sum([1 for a in self.table if a[2] == '5p']) + sum([1 for a in self.table if a[3] == '5p'])],
            ['Total 3p Ends Found',
             sum([1 for a in self.table if a[2] == '3p']) + sum([1 for a in self.table if a[3] == '3p'])],
            ['Total Trimmedat 5p', sum([1 for a in self.table if a[4]])],
            ['Total Trimmedat 3p', sum([1 for a in self.table if a[5]])],
            ['Total Deleted', sum([1 for a in self.table if a[6]])],
            ['Seq after derep.', self.num_seq_after_dereplication if self.num_seq_after_dereplication else 'N/A']
        ]

        return DataFrame(out, columns=['Cat.', '#'])

    def iter_dict(self):
        for row in self.table:
            yield {col_name: row[col_idx] for col_idx, col_name in enumerate(self.columns)}

    def out_as_csv(self, file):
        writer = DictWriter(file, fieldnames=self.columns)
        writer.writeheader()
        for d in self.iter_dict():
            writer.writerow(d)

    def complete_clustered_with(self, clusters: dict):
        feature_ids = [i[0] for i in self.table]
        for cl, member in clusters.items():
            if len(member) > 0:
                self.table[feature_ids.index(cl)][7] = ' '.join(member)
                for m in member:
                    self.table[feature_ids.index(m)][7] = cl

        self.num_seq_after_dereplication = len(clusters)

    def as_DataFrame(self):
        df = DataFrame(self.table, columns=self.columns, dtype=str)
        df.set_index('feature-id', inplace=True)
        return df


class Direction(Enum):
    """
    Enumeration of read orientation
    """
    unknown = 0
    sens = 1
    antisens = 2


class Ends(Enum):
    """
    Enumeration of DNA ends
    """
    _5p = 0
    _3p = 1


class PredefinedPrimers(Enum):
    """
    Enumeration of classically used primers in the literature
    """
    U515F = 'GTGCCAGCMGCCGCGGTAA'
    U806R = 'GGACTACVSGGGTATCTAAT'
    PK_926R = 'CCGYCAATTYMTTTRAGTTT'
    EK_565F = 'GCAGTTAAAAAGCTCGTAGT'
    UNonMet = 'TTTAAGTTTCAGCCTTGCG'


class RegionsReference(Enum):
    """
    Enumeration of classical amplicon regions from the literature
    """
    SSU_16S_V4 = 'SSU_16S_V4'
    SSU_18S_V4 = 'SSU_18S_V4'
    FILE = 'USE_FILE'


RegionsReferenceData = {
    RegionsReference.SSU_16S_V4:
        __assets__ / 'SSU_16S_V4_reference.fasta',
    RegionsReference.SSU_18S_V4:
        __assets__ / 'SSU_18S_V4_reference.fasta'

}
