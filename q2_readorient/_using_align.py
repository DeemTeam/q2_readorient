#  ---------------------- ReadOrient -------------------------  #
#  A plugin for Qiime2 to reorient amplicon derived features    #
#  using the primer sequences that were used for amplification  #
#                                                               #
#  Author : Philippe Deschamps <philippe.fields@gmail.com>      #
#                                                               #
#  Distributed under the license terms of Qiime2                #
#  https://qiime2.org/                                          #
#  -----------------------------------------------------------  #

# Imports from libraries
from typing import TextIO
import skbio
from biom import Table as biomTable
from pathlib import Path
from joblib import Parallel, delayed
# Imports from Qiime
from q2_types.feature_data import DNAFASTAFormat
from qiime2 import Metadata
# Internal Imports
from q2_readorient._types import Direction, ReadOrientStatsObj, RegionsReference, RegionsReferenceData
from q2_readorient._vsearch import _generate_input_file, _cluster_100, _derep_fulllength
from q2_readorient._biom import dereplicate_table
from q2_readorient._mafft import _mafft_adjust_direction, _mafft_prepare_input_file


def features_w_table_align(features: DNAFASTAFormat,
                           features_table: biomTable,
                           region: str = 'USE_FILE',
                           reference_sequence: str = None,
                           num_threads: int = 1,
                           dereplicate: bool = True
                           ) -> (DNAFASTAFormat, biomTable, Metadata):
    # Checking options
    _region_reference = RegionsReference(region)
    if _region_reference is RegionsReference.FILE:
        if not reference_sequence:
            raise ValueError("Region is set to `USE_FILE` but no file path provided")
        else:
            _reference_sequence_path = Path(reference_sequence)
            if not _reference_sequence_path.exists():
                raise FileExistsError("The path th reference sequences does not exist")
    else:
        _reference_sequence_path = RegionsReferenceData[_region_reference]

    # reorient amplicons
    reoriented_sequences, stats = _reorient_amplicon_mafft(features, _reference_sequence_path, num_threads)
    if dereplicate:
        # Preparing for vsearch
        vsearch_input_file = _generate_input_file(reoriented_sequences, features_table)
        # Dereplicating sequences
        clusters_data, uc_file, clustered_featured_file = _derep_fulllength(vsearch_input_file, num_threads)
        # Dereplicating biom table
        derep_biom_table = dereplicate_table(features_table, clusters_data)
        stats.complete_clustered_with(clusters_data)

    else:
        clustered_featured_file = reoriented_sequences
        derep_biom_table = features_table

    # Setting up Report Dir
    stats_metadata = Metadata(stats.as_DataFrame())
    print(stats.recap())

    return clustered_featured_file, derep_biom_table, stats_metadata


def _reorient_amplicon_mafft(amplicon_file: DNAFASTAFormat,
                             reference_file: Path,
                             num_threads: int = 1,
                             ) -> (DNAFASTAFormat, ReadOrientStatsObj):  # (output_fasta, stats)
    """
        Use mafft to align amplicons and adjust their direction
        :param amplicon_file: the file containing the amplicons to reorient
        :param reference_file: a reference file containing the same type of amplicon in the correct orientation
        :return: DNAFASTAFormat, ReadOrientStatsObj
    """
    reoriented_sequences = DNAFASTAFormat()
    stats = ReadOrientStatsObj()

    # Prepare reference data :
    _ref_seq_container = []
    for seq in skbio.io.read(str(reference_file.absolute()), constructor=skbio.DNA, format='fasta'):
        _ref_seq_container.append(seq)

    _worker_files = [(in_f, DNAFASTAFormat()) for in_f in _mafft_prepare_input_file(amplicon_file, _ref_seq_container)]
    # Aligning input files
    if num_threads == 1:
        for (in_fn, out_f) in _worker_files:
            _mafft_adjust_direction(in_fn, out_f)
    else:
        Parallel(n_jobs=num_threads, prefer="threads")(delayed(_mafft_adjust_direction)(in_fn, out_f) for (in_fn, out_f) in _worker_files)

    with open(str(reoriented_sequences), 'w') as output_fasta_f:
        # Getting headers from reference sequences
        reference_headers = [s.metadata['id'] for s in _ref_seq_container]
        # iterating over output Fasta files
        for (in_fn, out_f) in _worker_files:
            for seq in skbio.io.read(str(out_f), constructor=skbio.DNA, format='fasta', lowercase=True):
                # Determine direction
                direction = Direction.antisens if str(seq.metadata['id']).find('_R_') == 0 else Direction.sens
                # Strip '_R_' if needed
                seq.metadata['id'] = str(seq.metadata['id']).strip('_R_')

                # Check that sequence is not reference sequence
                if seq.metadata['id'] not in reference_headers:
                    curr_stat_row = stats.row_template()
                    curr_stat_row['feature-id'] = seq.metadata['id']
                    curr_stat_row['orientation'] = 'sens' if direction == Direction.sens else 'anti'

                    # Outputting
                    # _out(output_fasta_f, seq, direction)
                    output_fasta_f.write('>{}\n{}\n'.format(seq.metadata['id'],
                                                            str(seq.degap())
                                                            )
                                         )

                    stats.add_row(*[curr_stat_row[col] for col in stats.columns])

    return reoriented_sequences, stats


# def _out(file: TextIO,
#          seq: skbio.DNA,
#          direction: Direction):
#     """
#     Write single sequence to fasta file
#     :param file: File handler to write into
#     :param seq: The sequence of the amplicon to write
#     :param direction: the direction of the amplicon
#     """
#     ungapped = seq.degap()
#     if direction == Direction.sens:
#         file.write('>{}\n{}\n'.format(ungapped.metadata['id'],
#                                       str(ungapped)
#                                       )
#                    )
#     elif direction == Direction.antisens:
#         print('complement')
#         file.write('>{}\n{}\n'.format(ungapped.metadata['id'],
#                                       str(ungapped)
#                                       # str(ungapped.reverse_complement())
#                                       )
#                    )