#  ---------------------- ReadOrient -------------------------  #
#  A plugin for Qiime2 to reorient amplicon derived features    #
#  using the primer sequences that were used for amplification  #
#                                                               #
#  Author : Philippe Deschamps <philippe.fields@gmail.com>      #
#                                                               #
#  Distributed under the license terms of Qiime2                #
#  https://qiime2.org/                                          #
#  -----------------------------------------------------------  #

# Imports from libraries
import skbio
from biom import Table as biomTable
import logging
# Imports from Qiime
from q2_types.feature_data import DNAFASTAFormat
from qiime2 import Metadata
# Internal Imports
from q2_readorient._types import Primer, Direction, Ends, ReadOrientStatsObj
from q2_readorient._vsearch import _generate_input_file, _cluster_100
from q2_readorient._biom import dereplicate_table


# ------ MAIN FUNCTIONS ------ #
def features_w_table_primer(features: DNAFASTAFormat,
                            features_table: biomTable,
                            primer_fwd: str,
                            primer_rev: str,
                            trim_primers: bool = True,
                            keep_unkn: bool = False,
                            dereplicate: bool = True
                            ) -> (DNAFASTAFormat, biomTable, Metadata):  # , ReadOrientReport) :

    primer_fwd = Primer('FWD', primer_fwd)
    primer_rev = Primer('REV', primer_rev)
    # total_Seq = sum([1 for i in skbio.io.read(sequences, constructor=skbio.DNA, format='fasta')])
    # print(primer_fwd, primer_rev, total_Seq)

    # reorient
    reoriented_sequences, stats = _reorient_amplicon_both_ends(features,
                                                               primer_fwd,
                                                               primer_rev,
                                                               trim_primers,
                                                               keep_unkn
                                                               )
    # Preparing for vsearch
    vsearch_input_file = _generate_input_file(reoriented_sequences, features_table)
    # Dereplicating sequences
    clusters_data, uc_file, clustered_featured_file = _cluster_100(vsearch_input_file)
    # Dereplicating biom table
    derep_biom_table = dereplicate_table(features_table, clusters_data)
    stats.complete_clustered_with(clusters_data)
    # Setting up Report Dir
    stats_metadata = Metadata(stats.as_DataFrame())

    print(stats.recap())

    return clustered_featured_file, derep_biom_table, stats_metadata


def _reg_search(seq, pat, end=Ends._5p):
    coords = [m for m in seq.find_with_regex(pat)]
    if len(coords) > 0:
        if end == Ends._5p:
            if coords[0].start < 20:
                return coords[0]
            else:
                return False
        elif end == Ends._3p:
            if coords[0].stop > len(seq) - 20:
                return coords[0]
            else:
                return False
        else:
            raise ValueError("End has to be 5p ou 3p")
    else:
        return False


def _out(file, seq, direction, coords_5p, coords_3p, keep_unkn, stat_row, ask_both):
    """
    Write single sequence to fasta file
    """
    if direction == Direction.sens:
        file.write('>{}\n{}\n'.format(seq.metadata['id'],
                                      str(seq)
                                      )
                   )
    elif direction == Direction.antisens:
        file.write('>{}\n{}\n'.format(seq.metadata['id'],
                                      str(seq.reverse_complement())
                                      )
                   )
    elif keep_unkn:
        file.write('>{}\n{}\n'.format(seq.metadata['id'],
                                      str(seq)
                                      )
                   )
    else:
        stat_row['deleted'] = True


def _trim_and_out(file, seq, direction, coords_5p, coords_3p, keep_unkn, stat_row, ask_both):
    """
    Trim seq and send to _out()
    """
    if coords_3p:
        seq = seq[:coords_3p.start]
        stat_row['trimAt3p'] = True
    if coords_5p:
        seq = seq[coords_5p.stop:]
        stat_row['trimAt5p'] = True
    if ask_both:
        if coords_3p and coords_5p:
            _out(file, seq, direction, coords_5p, coords_3p, keep_unkn, stat_row, ask_both)
        else:
            stat_row['deleted'] = True
    else:
        _out(file, seq, direction, coords_5p, coords_3p, keep_unkn, stat_row, ask_both)


def _reorient_amplicon_single_end(sequences: DNAFASTAFormat,
                                  primer_fwd: Primer,
                                  primer_rev: Primer,
                                  trim_primers: bool = False,
                                  keep_unkn: bool = True
                                  ) -> (DNAFASTAFormat, ReadOrientStatsObj):  # (output_fasta, stats)
    """ Will search only on 5' end for primers :
            - sens : should find primer_fwd_sens
            - antisens : shoud find primer_rev_sens
        Reorient if found in antisens
        Trim primer if option set
    """

    reoriented_sequences = DNAFASTAFormat()
    stats = ReadOrientStatsObj()
    ask_both = False

    if trim_primers and keep_unkn:
        logging.warning("Keep seq with unidentified ends is prohibited when trim is activated")
        keep_unkn = False

    # Set the output function depending on trim_primer parameter
    if trim_primers:
        _output = _trim_and_out
    else:
        _output = _out

    # Opening output file
    with open(str(reoriented_sequences), 'w') as output_fasta_f:
        # iterating over fasta
        for seq in skbio.io.read(str(sequences), constructor=skbio.DNA, format='fasta'):
            curr_stat_row = stats.row_template()
            curr_stat_row['feature-id'] = str(seq.metadata['id'])

            direction = Direction.unknown
            coords_5p = False
            coords_3p = False

            # Looking for primer_fwd_sens
            coords_5p = _reg_search(seq, primer_fwd.sens_pat, Ends._5p)
            if coords_5p:
                direction = Direction.sens
                curr_stat_row['orientation'] = 'sens'
                curr_stat_row['primer_fwd'] = '5p'
            # Looking for primer_rev_sens
            else:
                coords_5p = _reg_search(seq, primer_rev.sens_pat, Ends._5p)
                if coords_5p:
                    direction = Direction.antisens
                    curr_stat_row['orientation'] = 'anti'
                    curr_stat_row['primer_rev'] = '5p'
                    # Outputing
            _output(output_fasta_f, seq, direction, coords_5p, coords_3p, keep_unkn, curr_stat_row, ask_both)
            stats.add_row(*[curr_stat_row[col] for col in stats.columns])

    # returning DNAFASTAFormat file
    return (reoriented_sequences, stats)


def _reorient_amplicon_both_ends(sequences: DNAFASTAFormat,
                                 primer_fwd: Primer,
                                 primer_rev: Primer,
                                 trim_primers: bool = False,
                                 keep_unkn: bool = True
                                 ) -> (DNAFASTAFormat, ReadOrientStatsObj):
    """ Will search both 5' and 3' ends for primers :
            - sens : should find primer_fwd_sens on 5p 
                     and/or primer_rev_anti on 3p 
            - antisens : shoud find primer_rev_sens
                     and/or primer_fwd_anti on 3p
        Reorient if found in antisens
        Trim primer if option set
    """

    reoriented_sequences = DNAFASTAFormat()
    stats = ReadOrientStatsObj()
    ask_both = True

    if trim_primers and keep_unkn:
        logging.warning("Keep seq with unidentified ends is prohibited when trim is activated")
        keep_unkn = False

    # Set the output function depending on trim_primer parameter
    if trim_primers:
        _output = _trim_and_out
    else:
        _output = _out

    # Opening output file
    with open(str(reoriented_sequences), 'w') as output_fasta_f:
        # iterating over fasta
        for seq in skbio.io.read(str(sequences), constructor=skbio.DNA, format='fasta'):
            curr_stat_row = stats.row_template()
            curr_stat_row['feature-id'] = str(seq.metadata['id'])

            direction = Direction.unknown
            coords_5p = False
            coords_3p = False

            # ------ SEARCHING @ 5p ------ #
            # Looking for primer_fwd_sens
            coords_5p = _reg_search(seq, primer_fwd.sens_pat, Ends._5p)
            if coords_5p:
                direction = Direction.sens
                curr_stat_row['orientation'] = 'sens'
                curr_stat_row['primer_fwd'] = '5p'
            # Looking for primer_rev_sens
            else:
                coords_5p = _reg_search(seq, primer_rev.sens_pat, Ends._5p)
                if coords_5p:
                    direction = Direction.antisens
                    curr_stat_row['orientation'] = 'anti'
                    curr_stat_row['primer_rev'] = '5p'

            # ------ SEARCHING @ 3p ------ #
            # Looking for primer_fwd_anti
            coords_3p = _reg_search(seq, primer_rev.anti_pat, Ends._3p)
            if coords_3p:
                direction = Direction.sens
                curr_stat_row['orientation'] = 'sens'
                curr_stat_row['primer_rev'] = '3p'
            # Looking for primer_rev_anti
            else:
                coords_3p = _reg_search(seq, primer_fwd.anti_pat, Ends._3p)
                if coords_3p:
                    direction = Direction.antisens
                    curr_stat_row['orientation'] = 'anti'
                    curr_stat_row['primer_fwd'] = '3p'

            # Outputting
            _output(output_fasta_f, seq, direction, coords_5p, coords_3p, keep_unkn, curr_stat_row, ask_both)
            stats.add_row(*[curr_stat_row[col] for col in stats.columns])

    # returning DNAFASTAFormat file
    return (reoriented_sequences, stats)
