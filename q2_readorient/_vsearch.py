################################################################
#                                                               #
#  ---------------------- ReadOrient -------------------------  #
#  A plugin for Qiime2 to reorient amplicon derived features    #
#  using the primer sequences that were used for amplification  #
#                                                               #
#  Author : Philippe Deschamps <philippe.fields@gmail.com>      #
#                                                               #
#  Distributed under the license terms of Qiime2                #
#  https://qiime2.org/                                          #
#                                                               #
#################################################################

import skbio
import biom.table
from subprocess import call as pCall
from subprocess import run
from q2_types.feature_data import DNAFASTAFormat

from q2_readorient._biom import get_seq_to_count
from q2_readorient._formats import UsearchClusterFmt


def _generate_input_file(sequences: DNAFASTAFormat,
                         feature_table: biom.table
                         ) -> DNAFASTAFormat:
    seq_to_count = get_seq_to_count(feature_table)
    vsearch_input_file = DNAFASTAFormat()
    with open(str(vsearch_input_file), 'w') as output_fasta_f:
        for seq in skbio.io.read(str(sequences), constructor=skbio.DNA, format='fasta'):
            if seq.metadata['id'] in seq_to_count:
                output_fasta_f.write('>{};size={}\n{}\n'.format(
                    seq.metadata['id'],
                    seq_to_count[seq.metadata['id']],
                    str(seq)
                )
                )
    return vsearch_input_file


def _cluster_100(sequences: DNAFASTAFormat,
                 threads: int = 1
                 ) -> (dict, UsearchClusterFmt, DNAFASTAFormat):
    uc_file = UsearchClusterFmt()
    tmp_file = DNAFASTAFormat()
    clust_feat_file = DNAFASTAFormat()

    cmd = ['vsearch',
           '--threads', str(threads),
           '--cluster_fast', str(sequences),
           "--id", "1.0",
           "--uc", str(uc_file),
           "--centroids", str(tmp_file),
           "--quiet",
           "--qmask", "none"
           ]
    success = pCall(' '.join(cmd), shell=True)

    cluster_data = {cluster.split(';')[0]: [m.split(';')[0] for m in members] for cluster, members in
                    uc_file._dict_clusters().items()}

    with open(str(clust_feat_file), 'w') as ofObj:
        for seq in skbio.io.read(str(tmp_file), constructor=skbio.DNA, format='fasta'):
            ofObj.write('>{}\n{}\n'.format(seq.metadata['id'].split(';')[0],
                                           str(seq)
                                           )
                        )

    return cluster_data, uc_file, clust_feat_file


def _derep_fulllength(sequences: DNAFASTAFormat,
                      threads: int = 1
                      ) -> (dict, UsearchClusterFmt, DNAFASTAFormat):
    uc_file = UsearchClusterFmt()
    tmp_file = DNAFASTAFormat()
    clust_feat_file = DNAFASTAFormat()

    cmd = ['vsearch',
           '--threads', str(threads),
           '--derep_fulllength', str(sequences),
           "--uc", str(uc_file),
           "--output", str(tmp_file),
           "--quiet"
           ]

    _completed_process = run(' '.join(cmd), shell=True)
    if _completed_process.returncode != 0:
        raise RuntimeError("Command Failed : {}".format(' '.join(cmd)))

    cluster_data = {cluster.split(';')[0]: [m.split(';')[0] for m in members] for cluster, members in
                    uc_file._dict_clusters().items()}

    with open(str(clust_feat_file), 'w') as ofObj:
        for seq in skbio.io.read(str(tmp_file), constructor=skbio.DNA, format='fasta'):
            ofObj.write('>{}\n{}\n'.format(seq.metadata['id'].split(';')[0],
                                           str(seq)
                                           )
                        )

    return cluster_data, uc_file, clust_feat_file
