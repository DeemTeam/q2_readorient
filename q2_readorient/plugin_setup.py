#  ---------------------- ReadOrient -------------------------  #
#  A plugin for Qiime2 to reorient amplicon derived features    #
#  using the primer sequences that were used for amplification  #
#                                                               #
#  Author : Philippe Deschamps <philippe.fields@gmail.com>      #
#                                                               #
#  Distributed under the license terms of Qiime2                #
#  https://qiime2.org/                                          #
#  -----------------------------------------------------------  #

import qiime2.plugin
from qiime2 import Metadata
from q2_types.feature_data import FeatureData, Sequence
from q2_types.feature_table import FeatureTable, Frequency

# Internal Imports
from q2_readorient import features_w_table_primer, features_w_table_align, __version__
from q2_readorient._formats import UsearchClusterFmt, ReorientStatsFmt, ReadOrientStatsDirFmt
from q2_readorient._types import ReadOrientStats, RegionsReference

plugin = qiime2.plugin.Plugin(
    name='readorient',
    version=__version__,
    website='https://gitlab.com/DeemTeam/q2_readorient',
    package='q2_readorient',
    description='This QIIME reorients amplicon derived features using'
                'the primer sequences that were used for amplification',
    short_description='A plugin to reaorient amplicon features'
    # ,
    # citations=Citations.load('citations.bib', package='q2_readorient')
)

plugin.register_formats(UsearchClusterFmt, ReorientStatsFmt, ReadOrientStatsDirFmt)
plugin.register_semantic_types(ReadOrientStats)
plugin.register_semantic_type_to_format(
    FeatureData[ReadOrientStats],
    artifact_format=ReadOrientStatsDirFmt)


@plugin.register_transformer
def _1(ff: ReorientStatsFmt) -> Metadata:
    return Metadata.load(str(ff))


@plugin.register_transformer
def _2(obj: Metadata) -> ReorientStatsFmt:
    ff = ReorientStatsFmt()
    obj.save(str(ff))
    return ff


plugin.methods.register_function(
    function=features_w_table_primer,
    inputs={
        'features': FeatureData[Sequence],
        'features_table': FeatureTable[Frequency]},
    parameters={
        'primer_fwd': qiime2.plugin.Str,
        'primer_rev': qiime2.plugin.Str,
        'trim_primers': qiime2.plugin.Bool,
        'keep_unkn': qiime2.plugin.Bool,
        'dereplicate': qiime2.plugin.Bool
    },
    outputs=[
        ('reoriented_features', FeatureData[Sequence]),
        ('derep_feature_table', FeatureTable[Frequency]),
        ('readorient_stats', FeatureData[ReadOrientStats]),
    ],
    input_descriptions={
        'features': 'The feature sequences to be reoriented.',
        'features_table': ('Feature table, will be used for '
                           're-clustering.'),
    },
    parameter_descriptions={
        'primer_fwd': 'Forward primer sequence used for amplification',
        'primer_rev': 'Reverse primer sequence used for amplification',
        'trim_primers': 'Trim primers sequence from feature',
        'keep_unkn': ('Keep features for which orientation could ot be determined '
                      '(not compatible with trim_primers)'),
        'dereplicate': 'Dereplicate identical features after reorientation'
    },
    output_descriptions={
        'reoriented_features': ('Features sequences after reorientation '
                                ' (optionally dereplicated)'),
        'derep_feature_table': ('Features table after reorientation '
                                ' (optionally dereplicated)'),
        'readorient_stats': 'Report containing reorentation and dereplication stats'
    },
    name='Reorient and optionally clusters feature sequences using primers',
    description=('Reads denoising and asv merging made with dada2 '
                 'does not take into account the orientation for the reads '
                 'leading to identical asv in opposite directions, and thus'
                 'artificially doubling the number of OTUs in features table. '
                 'This plugin offers a solution by reorienting asv and '
                 'optionnally clustering identical asv ')
)

plugin.methods.register_function(
    function=features_w_table_align,
    inputs={
        'features': FeatureData[Sequence],
        'features_table': FeatureTable[Frequency]},
    parameters={
        'region': qiime2.plugin.Str % qiime2.plugin.Choices([e.value for e in RegionsReference]),
        'reference_sequence': qiime2.plugin.Str,
        'num_threads': qiime2.plugin.Int,
        'dereplicate': qiime2.plugin.Bool
    },
    outputs=[
        ('reoriented_features', FeatureData[Sequence]),
        ('derep_feature_table', FeatureTable[Frequency]),
        ('readorient_stats', FeatureData[ReadOrientStats]),
    ],
    input_descriptions={
        'features': 'The feature sequences to be reoriented.',
        'features_table': ('Feature table, will be used for '
                           're-clustering.'),
    },
    parameter_descriptions={
        'region': ('Use literature-based reference region for alignment '
                   '(not compatible with `reference_sequence`)'),
        'reference_sequence': ('Path to file containing reference sequences (correctly oriented) for alignment '
                               '(not compatible with `region`)'),
        'num_threads': 'Parallel execution on n threads',
        'dereplicate': 'Dereplicate identical features after reorientation'
    },
    output_descriptions={
        'reoriented_features': ('Features sequences after reorientation '
                                ' (optionally dereplicated)'),
        'derep_feature_table': ('Features table after reorientation '
                                ' (optionally dereplicated)'),
        'readorient_stats': 'Report containing reorentation and dereplication stats'
    },
    name='Reorient and optionally clusters feature sequences using alignments',
    description=('Reads denoising and asv merging made with dada2 '
                 'does not take into account the orientation for the reads '
                 'leading to identical asv in opposite directions, and thus'
                 'artificially doubling the number of OTUs in features table. '
                 'This plugin offers a solution by reorienting asv and '
                 'optionnally clustering identical asv ')
)
