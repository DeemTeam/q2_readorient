#################################################################
#                                                               #
#  ---------------------- ReadOrient -------------------------  #
#  A plugin for Qiime2 to reorient amplicon derived features    #
#  using the primer sequences that were used for amplification  #
#                                                               #
#  Author : Philippe Deschamps <philippe.fields@gmail.com>      #
#                                                               #
#  Distributed under the license terms of Qiime2                #
#  https://qiime2.org/                                          #
#                                                               #
#################################################################

from q2_readorient._using_primers import (
    _reorient_amplicon_both_ends,
    _reorient_amplicon_single_end
)
from q2_readorient._types import (
    Primer,
    PredefinedPrimers
)

Sample_data = {
    'features_not_oriented':
        "q2_readorient/tests/data/features_not_oriented.fasta",
    'features_oriented':
        "q2_readorient/tests/data/features_oriented.fasta",
    'features_oriented_w_sizes':
        "q2_readorient/tests/data/features_oriented_w_sizes.fasta",
    'feature_table':
        "q2_readorient/tests/data/feature_table.biom",
    'feature_table_derep':
        "q2_readorient/tests/data/feature_table_derep.biom",
    'features_clustered':
        "q2_readorient/tests/data/features_clustered.fasta",
    'sample_features_fasta':
        "q2_readorient/tests/data/sample_features.fasta",
    'clusters': {'EK_565F_sens_1': ['UNonMet_sens_1'],
                 'EK_565F_sens_2': ['UNonMet_sens_2'],
                 'EK_565F_sens_3': []}
}

feature_sample_tests = {
    'input': "q2_readorient/tests/data/sample_features.fasta",
    'outputs': {
        'features_both_ends_noTrim_KeepUnkn':
            {'function': _reorient_amplicon_both_ends,
             'primer_fwd': Primer('FWD', PredefinedPrimers.EK_565F.value),
             'primer_rev': Primer('REV', PredefinedPrimers.UNonMet.value),
             'trim_primers': False,
             'keep_unkn': True,
             'fasta_out': "q2_readorient/tests/data/sample_features_both_ends_noTrim_KeepUnkn.fasta",
             'stats_out': "q2_readorient/tests/data/sample_features_both_ends_noTrim_KeepUnkn.stats"
             },
        'features_both_ends_noTrim_noKeepUnkn':
            {'function': _reorient_amplicon_both_ends,
             'primer_fwd': Primer('FWD', PredefinedPrimers.EK_565F.value),
             'primer_rev': Primer('REV', PredefinedPrimers.UNonMet.value),
             'trim_primers': False,
             'keep_unkn': False,
             'fasta_out': "q2_readorient/tests/data/sample_features_both_ends_noTrim_noKeepUnkn.fasta",
             'stats_out': "q2_readorient/tests/data/sample_features_both_ends_noTrim_noKeepUnkn.stats"
             },
        'features_both_ends_Trim_KeepUnkn':
            {'function': _reorient_amplicon_both_ends,
             'primer_fwd': Primer('FWD', PredefinedPrimers.EK_565F.value),
             'primer_rev': Primer('REV', PredefinedPrimers.UNonMet.value),
             'trim_primers': True,
             'keep_unkn': True,
             'fasta_out': "q2_readorient/tests/data/sample_features_both_ends_Trim_KeepUnkn.fasta",
             'stats_out': "q2_readorient/tests/data/sample_features_both_ends_Trim_KeepUnkn.stats"
             },
        'features_both_ends_Trim_noKeepUnkn':
            {'function': _reorient_amplicon_both_ends,
             'primer_fwd': Primer('FWD', PredefinedPrimers.EK_565F.value),
             'primer_rev': Primer('REV', PredefinedPrimers.UNonMet.value),
             'trim_primers': True,
             'keep_unkn': False,
             'fasta_out': "q2_readorient/tests/data/sample_features_both_ends_Trim_noKeepUnkn.fasta",
             'stats_out': "q2_readorient/tests/data/sample_features_both_ends_Trim_noKeepUnkn.stats"
             },
        'features_single_end_noTrim_keepUnkn':
            {'function': _reorient_amplicon_single_end,
             'primer_fwd': Primer('FWD', PredefinedPrimers.EK_565F.value),
             'primer_rev': Primer('REV', PredefinedPrimers.UNonMet.value),
             'trim_primers': False,
             'keep_unkn': True,
             'fasta_out': "q2_readorient/tests/data/sample_features_single_end_noTrim_keepUnkn.fasta",
             'stats_out': "q2_readorient/tests/data/sample_features_single_end_noTrim_keepUnkn.stats"
             },
        'features_single_end_noTrim_noKeepUnkn':
            {'function': _reorient_amplicon_single_end,
             'primer_fwd': Primer('FWD', PredefinedPrimers.EK_565F.value),
             'primer_rev': Primer('REV', PredefinedPrimers.UNonMet.value),
             'trim_primers': False,
             'keep_unkn': False,
             'fasta_out': "q2_readorient/tests/data/sample_features_single_end_noTrim_noKeepUnkn.fasta",
             'stats_out': "q2_readorient/tests/data/sample_features_single_end_noTrim_noKeepUnkn.stats"
             },
        'features_single_end_Trim_KeepUnkn':
            {'function': _reorient_amplicon_single_end,
             'primer_fwd': Primer('FWD', PredefinedPrimers.EK_565F.value),
             'primer_rev': Primer('REV', PredefinedPrimers.UNonMet.value),
             'trim_primers': True,
             'keep_unkn': True,
             'fasta_out': "q2_readorient/tests/data/sample_features_single_end_Trim_KeepUnkn.fasta",
             'stats_out': "q2_readorient/tests/data/sample_features_single_end_Trim_KeepUnkn.stats"
             },
        'features_single_end_Trim_noKeepUnkn':
            {'function': _reorient_amplicon_single_end,
             'primer_fwd': Primer('FWD', PredefinedPrimers.EK_565F.value),
             'primer_rev': Primer('REV', PredefinedPrimers.UNonMet.value),
             'trim_primers': True,
             'keep_unkn': False,
             'fasta_out': "q2_readorient/tests/data/sample_features_single_end_Trim_noKeepUnkn.fasta",
             'stats_out': "q2_readorient/tests/data/sample_features_single_end_Trim_noKeepUnkn.stats"
             }
    }
}

dada_out_fasta = "q2_readorient/tests/data/dada-out/dada-rep-seqs/dna-sequences.fasta"
