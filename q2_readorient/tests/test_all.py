#################################################################
#                                                               #
#  ---------------------- ReadOrient -------------------------  #
#  A plugin for Qiime2 to reorient amplicon derived features    #
#  using the primer sequences that were used for amplification  #
#                                                               #
#  Author : Philippe Deschamps <philippe.fields@gmail.com>      #
#                                                               #
#  Distributed under the license terms of Qiime2                #
#  https://qiime2.org/                                          #
#                                                               #
#################################################################

# Import from libs
import unittest
from json import loads
# Self imports
from q2_readorient._types import Primer, ReadOrientStatsObj
from q2_readorient.tests import Sample_data, feature_sample_tests
from q2_readorient._vsearch import _generate_input_file, _cluster_100
from q2_readorient._biom import load_biom_table, dereplicate_table


class test_(unittest.TestCase):
    def setUp(self):
        self.Sample_data = Sample_data
 
    def test1_instantiation(self):
        print("Testing types instance")
        Primer('Test', 'ATCGCCGTGTAAA')
        ReadOrientStatsObj()

    def test2_reorient(self):
        print("Testing _reorient")
        test_input = feature_sample_tests['input']
        test_outputs = feature_sample_tests['outputs']
        for name, param in test_outputs.items():
            print(name)
            output_fasta, stats = param['function'](
                                        test_input, 
                                        param['primer_fwd'],
                                        param['primer_rev'],
                                        trim_primers = param['trim_primers'],
                                        keep_unkn = param['keep_unkn']
                                        )
            # comparing fastas
            with open(str(output_fasta)) as test_f, open(param['fasta_out']) as ref_f: 
                self.assertListEqual(list(test_f), list(ref_f))

            # comparing stats
            # with open(param['stats_out']) as ref_f:
            #     ref_stats = ref_f.read()
            #     self.assertEqual(str(stats), ref_stats)
            
            #print(stats.as_DataFrame())

        # self.assertListEqual(
        #     list(io.open(stats)),
        #     list(io.open()))

    def test3_vsearch_generate_input_file(self):
        biom_table = load_biom_table(self.Sample_data['feature_table'])
        
        output_fasta_name = _generate_input_file(   
                                self.Sample_data['features_oriented'],
                                biom_table
                                )

        with open(str(output_fasta_name)) as output_fasta_f:
            gen_content = output_fasta_f.read()
        with open(self.Sample_data['features_oriented_w_sizes']) as ref_fasta_f:
            ref_content = ref_fasta_f.read() 

        self.assertEqual(gen_content, ref_content)

    def test4_cluster_100(self):
        clusters, uc_file, clust_feat_file =_cluster_100(self.Sample_data['features_oriented_w_sizes'])
        self.assertEqual(clusters, self.Sample_data['clusters'])

        with open(str(clust_feat_file)) as output_fasta_f:
            gen_content = output_fasta_f.read()
        with open(self.Sample_data['features_clustered']) as ref_fasta_f:
            ref_content = ref_fasta_f.read()

        #print(gen_content, ref_content)
        self.assertEqual(gen_content, ref_content)

    def test5_dereplicate_biom_table(self):
        old_biom_table = load_biom_table(self.Sample_data['feature_table'])
        new_biom_table = load_biom_table(self.Sample_data['feature_table_derep']) 
        
        computed_biom_table = dereplicate_table(old_biom_table, self.Sample_data['clusters'])

        nbt_json = loads(new_biom_table.to_json('Qimme2 - q2_reorient', direct_io=None) )
        cbt_json = loads(computed_biom_table.to_json('Qimme2 - q2_reorient', direct_io=None) )

        self.assertEqual(nbt_json['data'], cbt_json['data'])
        self.assertEqual(nbt_json['rows'], cbt_json['rows'])
        self.assertEqual(nbt_json['columns'], cbt_json['columns'])

    def test_6_reorient_amplicon_mafft(self):
        pass


if __name__ == '__main__':
    unittest.main()