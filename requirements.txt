setuptools~=49.6.0
scikit-bio~=0.5.6
numpy~=1.20.2
qiime2~=2021.4.0
click~=7.1.2
pandas~=1.2.4
joblib~=1.0.1
importlib-resources~=5.4.0